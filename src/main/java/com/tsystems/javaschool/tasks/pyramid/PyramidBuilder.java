package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public static int  checkCount(int n, int maxIterCount){    // выводит кол-во чисел в основании пирамиды или -1, если построить невозможно
        int countForPyramid = 0;
        for (int i = 1; i < maxIterCount; i++) {
            countForPyramid+=i;
            if(n==countForPyramid)
                return i;
        }
        return -1;
    }

    public static void printMatrix(int[][] matrix,int h, int base){
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < base; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void matrixInit(int[][] matrix,int h, int base){
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < base; j++) {
                matrix[i][j] = 0;
            }
        }
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        for(Integer it : inputNumbers){
            if(it==null) throw new CannotBuildPyramidException("Exception: it's not possible to build one");;
        }

        int base = checkCount(inputNumbers.size(), 10000);    // кол-во чисел в основании пирамиды
        int high = base;    // высота матрицы
        if (base == -1)
            throw new CannotBuildPyramidException("Exception: it's not possible to build one");
        inputNumbers.sort(Integer::compareTo);  // сортируем список

        base *= 2;
        base--; // полный размер базы с нулями


        int[][] matrix = new int[high][base];
        matrixInit(matrix, high, base);  // нужно ли?

        int countZeroSide = base / 2;   // кол-во нулей сбоку от пирамиды
        int it = 0;


        for (int j = 0; j < inputNumbers.size(); ) {
            for (int i = countZeroSide; i < base - countZeroSide; i += 2) {
                int temp = inputNumbers.get(j++);

                matrix[it][i] = temp;
            }
            it++;
            countZeroSide--;
        }
        printMatrix(matrix, high, base);


        return matrix;
    }


}
