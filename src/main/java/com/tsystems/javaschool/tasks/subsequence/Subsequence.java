package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if(x==null || y==null) throw new java.lang.IllegalArgumentException();
        int pointer = 0; // указывает на элемент, с которого следует вести поиск

        for(int i=0;i<x.size();i++){      // внешний цикл по эталонному массиву
            boolean fl = false; // флаг - найдена ли буква
            for(int j= pointer;j<y.size();j++){   // внутренний цикл по входному массиву

                if(x.get(i).equals(y.get(j))){
                    pointer = j+1;  //сдвигаем начала поиска за найденный элемент
                    fl = true;
                    break;  // остановка внутреннего цикла
                }
            }
            if(!fl) return false;  // если буква не найдена во входном массиве - false

        }
        return true;

    //    return false;
    }
}
