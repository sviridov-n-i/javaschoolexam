package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static boolean isOp(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private static byte opPrior(char op) {
        switch (op) {
            case '*':
            case '/':
                return 2;
        }
        return 1; // Тут остается + и -
    }

    public static String toPolish(String expression) {

        StringBuilder stack = new StringBuilder("");
        StringBuilder out = new StringBuilder("");

        char it, temp;

        for (int i = 0; i < expression.length(); i++) {
            it = expression.charAt(i);
            if (isOp(it)) {

                while (stack.length() > 0) {
                    temp = stack.substring(stack.length() - 1).charAt(0);

                    // если в стек добавляется менее приоритетный оператор, то в выходную строку добавляется старый(высокоприоритетный)
                    if (isOp(temp) && (opPrior(it) <= opPrior(temp))) {
                        out.append(" ").append(temp).append(" ");
                        stack.setLength(stack.length() - 1);
                    }
                    // иначе добавляем пробел, чтобы разделить цифры
                    else {
                        out.append(" ");
                        break;
                    }
                }
                out.append(" ");
                stack.append(it);


            }
            else if (it == '(') {
                stack.append(it);
            }
            else if (it == ')') {
                try{
                temp = stack.substring(stack.length()-1).charAt(0);
                while(temp != '('){
                    if (stack.length() < 1) {
                        return null;// Ошибка разбора скобок. Проверьте правильность выражения
                    }
                    out.append(" ").append(temp);
                    stack.setLength(stack.length()-1);
                    temp = stack.substring(stack.length()-1).charAt(0);
                }
                stack.setLength(stack.length()-1);
                }
                catch (Exception e){
                    return null;
                }

            }
            else {
                out.append(it); // Если символ не оператор - добавляем в выходную последовательность
            }
        }

        while (stack.length() > 0) {
            out.append(" ").append(stack.substring(stack.length()-1));
            stack.setLength(stack.length()-1);
        }

        return out.toString();
    }

    public String evaluate(String statement) {
        if(statement == null) return null;
        if(statement.isEmpty()) return null;
        statement = toPolish(statement);
        if(statement == null)
            return  null;
        double dA, dB;
        String sTmp;
        Deque<Double> stack = new ArrayDeque<Double>();
        StringTokenizer st = new StringTokenizer(statement);
        while(st.hasMoreTokens()) {
            try {
                sTmp = st.nextToken().trim();
                if (1 == sTmp.length() && isOp(sTmp.charAt(0))) {
                    if (stack.size() < 2) {
                        return null; // Неверное количество данных в стеке для операции
                    }
                    dB = stack.pop();
                    dA = stack.pop();
                    switch (sTmp.charAt(0)) {
                        case '+':
                            dA += dB;
                            break;
                        case '-':
                            dA -= dB;
                            break;
                        case '/':
                            dA /= dB;
                            break;
                        case '*':
                            dA *= dB;
                            break;
                        default:
                            return null;// Недопустимая операция
                    }
                    stack.push(dA);
                } else {
                    dA = Double.parseDouble(sTmp);
                    stack.push(dA);
                }
            } catch (Exception e) {
                return null; // Недопустимый символ в выражении
            }
        }

        if (stack.size() > 1) {
            return null; // Количество операторов не соответствует количеству операндов"
        }
        DecimalFormat df = new DecimalFormat("#.####");

        Double t = stack.pop();
        if(t.equals(Double.NEGATIVE_INFINITY) || t.equals(Double.POSITIVE_INFINITY))
            return null;

        String formatResult = df.format(t);
        formatResult = formatResult.replace(',','.');
        return formatResult;

    }

}
